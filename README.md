# twins_tracker

2 Feb 2017, 최근우

## 목적
같은 곡이지만 다른 id로 된 트랙을 찾아준다.

## 방법
`artist_name.lower()`와 `track_name.lower()`를 비교한다. 즉 이름을 기반으로 완전 일치하는것만 찾아준다.

## 실행
`main_match_names.py`를 실행

## 속도
`O(n^2)`라 그렇게 빠르진 않다. 아티스트 이름과 트랙 이름을 set()에 넣어서 처음 나온 경우엔 스트링 매치를 생략하는 방식으로 시간을 많이 아꼈다. 첫 5만줄까지는 1-2분내로 완료되지만 250k 전체는 2-3시간이 소요된다. 

## 결과
* 중복 개수/전체 곡 수
* 3,005/top 50k
* 9,625/100k
* 17,896/150k
* 27,869/200k 
* 38,829/250k

결과로 나오는 `same_track_different_id.txt`를 열면,

```
0hLH6q9Ii30ML0Xo4scX9H  The Cascades    5cqkI9eoKGe1PRhx5Sg9hD  Rhythm Of The Rain      6qF0CJP412Gd50Rn1sFg2o  The Cascades    7GHf8dQ2wA71wYOuRsQCZP  Rhythm Of The Rain
```
와 같이 정리되어있다. 포맷은
```
artist_id_1탭artist_name_1탭track_id_1탭track_name_1탭탭artist_id_2탭artist_name_2탭track_id_2탭track_name_2탭탭...
```
으로 되어있다. 즉..
```

with open('same_track_different_id', 'r') as f:
    for line in f:
        lines = line.split('\t')
        for sub_line in lines:
            artist_id, artist_name, track_id, track_name = sub_lint.split('\t')

```
으로 정보를 추출할 수 있다.

