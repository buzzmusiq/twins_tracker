import os
import sys
import pdb
import pprint


def main():
    ar_names = set()
    tr_names = set()
    ar_name_pairs = []
    same_pairs = []  # list of tuple (ar_name, tr_name)
    same_lines = []  # list of list of lines
    lines = []

    with open('artist_track_list_top250k_20161018.txt') as f_txt:
        for line_idx, line_now in enumerate(f_txt):

            if line_idx % 10000 == 0:
                print('{}-th line reading, {} twins so far'.format(line_idx, len(same_lines)))
                with open('same_track_different_id.txt', 'w') as f:
                    for two_line in same_lines:
                        for line in two_line:
                            f.write(line.replace('\n', '\t\t'))
                        f.write('\n')

            ar_id, ar_name, tr_id, tr_name = line_now.replace('\n', '').split('\t')
            # Rule: if two names in lowercase are identical --> same.
            ar_name = ar_name.lower()
            tr_name = tr_name.lower()

            ar_name_pairs.append((ar_name, tr_name))
            lines.append(line_now)

            if ar_name not in ar_names:
                ar_names.add(ar_name)
                tr_names.add(tr_name)
                continue  # it's a new artist, so why bother

            elif tr_name not in tr_names:
                tr_names.add(tr_name)
                continue  # same, but for track title.
            else:
                # print('candidate: {}, {}, {}'.format(line_idx, ar_name, tr_name))
                pass

            same_lines_here = []

            already_twins_exist = False
            for pair_idx, stored_pair in enumerate(same_pairs):
                if stored_pair == (ar_name, tr_name):
                    same_lines[pair_idx].append(line_now)
                    already_twins_exist = True
                    break

            if already_twins_exist:
                continue

            for pair_idx, (stored_pair, old_line) in enumerate(zip(ar_name_pairs[:-1], lines[:-1])):
                if stored_pair == (ar_name, tr_name):
                    same_lines_here.append(old_line)
                    break
            if not same_lines_here == []:
                same_lines_here.append(line_now)
                same_pairs.append((ar_name, tr_name))
                same_lines.append(same_lines_here)

    # Finally...
    with open('same_track_different_id.txt', 'w') as f:
        for two_line in same_lines:
            for line in two_line:
                f.write(line.replace('\n', '\t\t'))
            f.write('\n')
    print('Done! open same_track_different_id.txt')


if __name__ == '__main__':
    main()
